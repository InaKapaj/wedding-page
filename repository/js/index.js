function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  return array;
}

let details = ['resources/images/wedding-details/bells.jpg',
  'resources/images/wedding-details/cake.jpg', 'resources/images/wedding-details/candle.jpg',
  'resources/images/wedding-details/chair.jpg', 'resources/images/wedding-details/envelope.jpg',
  'resources/images/wedding-details/flowers.jpg', 'resources/images/wedding-details/love.jpg',
  'resources/images/wedding-details/pinetree.jpg', 'resources/images/wedding-details/place.jpg',
  'resources/images/wedding-details/placement.jpg', 'resources/images/wedding-details/shoes.jpg', 
  'resources/images/wedding-details/table.jpg', 'resources/images/wedding-details/wedding.jpg', 
  'resources/images/wedding-details/wood.jpg'];

let couple = ['resources/images/wedding-couple/bouqet.jpg', 'resources/images/wedding-couple/bride-bw.jpg',
  'resources/images/wedding-couple/bride-bw2.jpg', 'resources/images/wedding-couple/bride-in-wind.jpg',
  'resources/images/wedding-couple/bride.jpg', 'resources/images/wedding-couple/bride-detail.jpg',
  'resources/images/wedding-couple/bride-flower.jpg', 'resources/images/wedding-couple/bride-in-wind.jpg',
  'resources/images/wedding-couple/fireworks.jpg', 'resources/images/wedding-couple/groom-bw.jpg',
  'resources/images/wedding-couple/groom.jpg', 'resources/images/wedding-couple/hands.jpg',
  'resources/images/wedding-couple/ring.jpg', 'resources/images/wedding-couple/rings-flower.jpg',
  'resources/images/wedding-couple/suit.jpg'];

let guests = ['resources/images/wedding-guests/bridesmaids.jpg', 'resources/images/wedding-guests/bridesmaids2.jpg', 
  'resources/images/wedding-guests/church.jpg', 'resources/images/wedding-guests/guests-glass.jpg', 
  'resources/images/wedding-guests/guests-names.jpg', 'resources/images/wedding-guests/guests.jpg',
  'resources/images/wedding-guests/wedding-after-party.jpg', 'resources/images/wedding-guests/wedding-beer.jpg',
  'resources/images/wedding-guests/wedding-grandma.jpg']

details = shuffle(details);
couple = shuffle(couple);
guests = shuffle(guests)

window.onload = function() {
  let imageDetails = document.getElementById("wedding-details");
  for (let i = 0; i < details.length; i++) {
      let newImage = document.createElement("img");
      newImage.setAttribute("src", details[i]);
      newImage.setAttribute("height", "380");
      newImage.setAttribute("width", "360");
      imageDetails.append(newImage);
  }
  let imagesCouple = document.getElementById("wedding-couple");
  for (let j = 0; j < couple.length; j++) {
      let newImage = document.createElement("img");
      newImage.setAttribute("src", couple[j]);
      newImage.setAttribute("height", "380");
      newImage.setAttribute("width", "360");
      imagesCouple.append(newImage);
  }
  let imageGuests = document.getElementById("wedding-guests");
  for (let k = 0; k < guests.length; k++) {
      let newImage = document.createElement("img");
      newImage.setAttribute("src", guests[k]);
      newImage.setAttribute("height", "380");
      newImage.setAttribute("width", "360");
      imageGuests.append(newImage);
  }
}